INSTALL ANGULAR IN UBUNTU
--
### SET PROXY
Creating  an Apt Proxy Conf File

*  Create a new configuration file named proxy.conf.

sudo touch /etc/apt/apt.conf.d/proxy.conf

*  Open the proxy.conf file in a text editor.

sudo nano /etc/apt/apt.conf.d/proxy.conf

* Add the following line to set your HTTP and HTTPS proxy.

Acquire {
  HTTP::proxy "http://172.31.90.162:8080";
  HTTPS::proxy "http://172.31.90.162:8080";
}

(ctrl O to save + 
ENTER + 
ctrl X to exit)

In terminal

npm config set proxy http://172.31.90.162:8080

npm config set https-proxy http://172.31.90.162:8080

### Step 1 – Install Node.js

curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -

sudo apt-get install nodejs

### Step 2 – Install Angular/CLI

sudo npm install -g @angular/cli

### Step 3 – Create New Angular Application

ng new wth-web

### Step 4 – Serve Angular Application

cd wth-web

ng serve

### Others:

* Change host and port for running Angular application

ng serve --host 0.0.0.0 --port 8080

* Kill port

kill -9 $(lsof -i tcp:4200 -t)
