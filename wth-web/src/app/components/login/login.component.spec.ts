import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { LoginComponent } from './login.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { of, throwError } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { TypeAlertEnum } from 'src/app/models/TypeAlertEnum.model';

describe('LoginComponent', () => {
  let loginComponent: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let router;
  let navigateSpy;
  let loginServiceMock;
  const user: User = {
    id_user: 0,
    id_role: 4,
    username: '',
    password: '',
    person: {
      id_person: 0,
      first_name: '',
      last_name: '',
      birthday: '',
      gender: '',
      phone: 0,
      address_email: ''
    }
  };

  beforeEach(async(() => {
    loginServiceMock = jasmine.createSpyObj('LoginService', ['searchUser']);
    loginServiceMock.searchUser.and.returnValue(of(user));

    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      providers: [{ provide: LoginService, useValue: loginServiceMock }]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(LoginComponent);
        loginComponent = fixture.componentInstance;

        router = TestBed.get(Router);
        navigateSpy = spyOn(router, 'navigate');
      });
  }));

  it('#constructor, should create a login component', () => {
    expect(loginComponent).toBeTruthy();
  });

  it('#changeDisplay, should have a parameter displaySignInModal true', () => {
    expect(loginComponent.changeDisplay(true)).toBeUndefined();
  });

  it('#loginForm, should be a invalid login', () => {
    expect(loginComponent.loginForm.valid).toBeFalsy();
  });

  it('#loginForm, should be a valid login', () => {
    loginComponent.loginForm.setValue({ username: 'admin', password: 'admin' });
    expect(loginComponent.loginForm.valid).toBeTruthy();
  });

  it('#login, should be login correctly', () => {
    loginComponent.login();
    const actualResult = loginComponent.showMessage.type;
    const expectedResult: TypeAlertEnum = TypeAlertEnum.success;
    expect(actualResult).toBe(expectedResult);
  });

  it('#login, should be login uncorrectly', () => {
    loginServiceMock.searchUser.and.returnValue(
      throwError({
        message_error: 'Username or password incorrect'
      })
    );

    loginComponent.login();
    const actualResult = loginComponent.showMessage.type;
    const expectedResult: TypeAlertEnum = TypeAlertEnum.error;
    expect(actualResult).toBe(expectedResult);
  });

  it('#openRegister, should move to the register window', () => {
    loginComponent.openRegister();
    expect(navigateSpy).toHaveBeenCalledWith(['register']);
  });

  it('#openPatientHome, should move to the patientHome window', () => {
    loginComponent.openPatientHome();
    expect(navigateSpy).toHaveBeenCalledWith(['patient']);
  });
});
