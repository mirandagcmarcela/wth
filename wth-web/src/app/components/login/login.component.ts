import { Component } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { UserService } from '../../services/user.service';
import { Person } from 'src/app/models/person.model';
import { ShowAlert } from 'src/app/models/show-alert';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  styleUrls: ['login.component.css'],
  templateUrl: './login.component.html'
})
export class LoginComponent {
  public loginForm: FormGroup;
  public displaySignInModal = false;
  private person: Person = {
    id_person: 0,
    first_name: '',
    last_name: '',
    birthday: '',
    gender: '',
    phone: 0,
    address_email: ''
  };
  private user: User = {
    id_user: 0,
    id_role: 4,
    username: '',
    password: '',
    person: this.person
  };

  public showMessage = new ShowAlert();

  constructor(
    private loginService: LoginService,
    private router: Router,
    private userService: UserService
  ) {
    this.createLoginForm();
  }

  public createLoginForm(): void {
    this.loginForm = new FormGroup({
      username: new FormControl('', [
        Validators.minLength(5),
        Validators.maxLength(10),
        Validators.required
      ]),
      password: new FormControl('', [Validators.required])
    });
  }

  public login(): void {
    this.loginService.searchUser(this.loginForm.value).subscribe(
      (response: User) => {
        this.user = response;
        this.userService.setCurrentUser(this.user);
        this.showMessage.showMessageSuccess('Login Successfull');
        this.displaySignInModal = false;
        this.openPatientHome();
      },
      (error: HttpErrorResponse) => {
        this.showMessage.showMessageError(error.error.message_error);
      }
    );
  }

  public changeDisplay(display: boolean): void {
    this.displaySignInModal = display;
  }

  public openPatientHome(): void {
    this.router.navigate(['patient']);
  }

  public openRegister(): void {
    this.router.navigate(['register']);
  }
}
