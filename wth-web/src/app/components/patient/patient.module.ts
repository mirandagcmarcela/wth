import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { MedicalAppointmentComponent } from './components/medical-appointment/medical-appointment.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PatientRoutingModule } from './patient-routing.module';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [HomeComponent, MedicalAppointmentComponent],
  imports: [
    CommonModule,
    SharedModule,
    PatientRoutingModule,
    ReactiveFormsModule,
    FormsModule]
})
export class PatientModule {}
