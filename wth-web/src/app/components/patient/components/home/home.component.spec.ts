import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HomeComponent } from './home.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { UserServiceMock } from 'src/app/test-utils/user-service-mock';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

describe('HomeComponent', () => {
  let homeComponent: HomeComponent;
  let homeFixture: ComponentFixture<HomeComponent>;
  let router;
  let navigateSpy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeComponent],
      imports: [BrowserModule, HttpClientTestingModule, RouterTestingModule],
      providers: [{ provide: UserService, useClass: UserServiceMock }]
    })
      .compileComponents()
      .then(() => {
        homeFixture = TestBed.createComponent(HomeComponent);
        homeComponent = homeFixture.componentInstance;
        router = TestBed.get(Router);
        navigateSpy = spyOn(router, 'navigate');
      });
  }));

  it('#constructor, should create a home component', () => {
    expect(homeComponent).toBeTruthy();
  });

  it('#constructor, should have a user object', () => {
    expect(homeComponent.username).toEqual('admin');
  });

  it('#constructor, should have not a user object', () => {
    expect(homeComponent.user).toBeUndefined;
  });

  it('#createMedicalAppointment, should move to the patientAppointment window', () => {
    homeComponent.createMedicalAppointment();
    expect(navigateSpy).toHaveBeenCalledWith(['patient/appointment']);
  });
});
