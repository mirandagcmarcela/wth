import { Component } from '@angular/core';
import { UserService } from '../../../../services/user.service';
import { User } from 'src/app/models/user.model';
import { isNullOrUndefined } from 'util';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  styleUrls: ['home.component.css'],
  templateUrl: './home.component.html'
})
export class HomeComponent {
  username: string;
  user: User;

  constructor(
    private userService: UserService, 
    private router: Router,) {
    this.user = this.userService.getCurrentUser();
    if (!isNullOrUndefined(this.user)) {
      this.username = this.user.username;
    }
  }
  public createMedicalAppointment(): void{
    this.router.navigate(['patient/appointment']);
  }
}
