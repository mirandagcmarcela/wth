import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MedicalAppointmentComponent } from './medical-appointment.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SpecialtyService } from 'src/app/services/specialty.service';
import { SchedulesService } from 'src/app/services/schedule.service';
import { DoctorService } from './../../../../services/doctor.service';
import { of, throwError } from 'rxjs';
import { TypeAlertEnum } from 'src/app/models/TypeAlertEnum.model';
import { MedicalAppointmentService } from 'src/app/services/medical-appointment.service';

describe('MedicalAppointmentComponent', () => {
  let medicalAppointmentComponent: MedicalAppointmentComponent;
  let fixture: ComponentFixture<MedicalAppointmentComponent>;
  let doctorServiceMock;
  let specialtyServiceMock;
  let schedulesServiceMock;
  let medicalAppointmentServiceMock;
  const doctor = {
    first_name: 'Ana',
    id_doctor: 4,
    last_name: 'Soto'
  };

  beforeEach(async(() => {
    doctorServiceMock = jasmine.createSpyObj('DoctorService', [
      'searchDoctors'
    ]);
    doctorServiceMock.searchDoctors.and.returnValue(
      of([
        { id_doctor: 1, first_name: 'Anibal', last_name: 'Cruz' },
        { id_doctor: 2, first_name: 'Juan', last_name: 'Cruz' }
      ])
    );

    specialtyServiceMock = jasmine.createSpyObj('SpecialtyService', [
      'searchSpecialties'
    ]);
    specialtyServiceMock.searchSpecialties.and.returnValue(
      of([
        { id_specialty: 1, name: 'cardiology' },
        { id_specialty: 2, name: 'urology' }
      ])
    );

    schedulesServiceMock = jasmine.createSpyObj('SchedulesService', [
      'searchSchedules'
    ]);
    schedulesServiceMock.searchSchedules.and.returnValue(
      of([
        {available: true, end_time: '10:30:00', start_time: '10:00:00'},
        {available: true, end_time: '11:00:00', start_time: '10:30:00'},
      ])
    );

    medicalAppointmentServiceMock = jasmine.createSpyObj('MedicalAppointmentService', [
      'createMedicalAppointment'
    ]);
    medicalAppointmentServiceMock.createMedicalAppointment.and.returnValue(
      of({
        id_medicalappointment: 1,
        attention_date: '12-03-2020',
        schedule: '10:30',
        id_users: 5,
        id_doctor: 2
      })
    );

    TestBed.configureTestingModule({
      declarations: [MedicalAppointmentComponent],
      imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      providers: [
        { provide: DoctorService, useValue: doctorServiceMock },
        { provide: SpecialtyService, useValue: specialtyServiceMock },
        { provide: SchedulesService, useValue: schedulesServiceMock },
        { provide: MedicalAppointmentService, useValue: medicalAppointmentServiceMock }
      ]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(MedicalAppointmentComponent);
        medicalAppointmentComponent = fixture.componentInstance;
      });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalAppointmentComponent);
    medicalAppointmentComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('#constructor, should create a medicalAppointment component', () => {
    expect(medicalAppointmentComponent).toBeTruthy();
  });

  it('#appointmentForm, should be a invalid registry', () => {
    expect(medicalAppointmentComponent.appointmentForm.valid).toBeFalsy();
  });

  it('#appointmentForm, should be a valid registry', () => {
    medicalAppointmentComponent.appointmentForm.setValue({
      id_medicalappointment: 1,
      attention_date: '12-03-2020',
      schedule: '10:30',
      id_users: 5,
      id_doctor: 2
    });
    expect(medicalAppointmentComponent.appointmentForm.valid).toBeTruthy();
  });

  it('#searchSpecialties, should get specialties correctly', () => {
    medicalAppointmentComponent.specialties$ = of([
      { id_specialty: 1, name: 'cardiology' },
      { id_specialty: 2, name: 'urology' }
    ]);
    medicalAppointmentComponent.searchSpecialties();
    medicalAppointmentComponent.specialties$.subscribe(x =>
      expect(x).toEqual([
        { id_specialty: 1, name: 'cardiology' },
        { id_specialty: 2, name: 'urology' }
      ])
    );
  });

  it('#searchSpecialties, should get specialties incorrectly', () => {
    specialtyServiceMock.searchSpecialties.and.returnValue(
      throwError('An error occurred')
    );
    medicalAppointmentComponent.searchDoctors();
    const actualResult = medicalAppointmentComponent.showMessage.type;
    const expectedResult: TypeAlertEnum = TypeAlertEnum.error;
    expect(actualResult).toBe(expectedResult);
  });

  it('#searchDoctors, should get doctors correctly', () => {
    medicalAppointmentComponent.doctors$ = of([
      { id_doctor: 1, first_name: 'Anibal', last_name: 'Cruz' },
      { id_doctor: 2, first_name: 'Juan', last_name: 'Cruz' }
    ]);
    medicalAppointmentComponent.searchDoctors();
    medicalAppointmentComponent.doctors$.subscribe(x =>
      expect(x).toEqual([
        { id_doctor: 1, first_name: 'Anibal', last_name: 'Cruz' },
        { id_doctor: 2, first_name: 'Juan', last_name: 'Cruz' }
      ])
    );
  });

  it('#searchDoctors, should get doctors incorrectly', () => {
    doctorServiceMock.searchDoctors.and.returnValue(
      throwError('An error occurred')
    );
    medicalAppointmentComponent.searchDoctors();
    const actualResult = medicalAppointmentComponent.showMessage.type;
    const expectedResult: TypeAlertEnum = TypeAlertEnum.error;
    expect(actualResult).toBe(expectedResult);
  });

  it('#getFullNameDoctor, should return the doctor name', () => {
    const actualResult = medicalAppointmentComponent.getFullNameDoctor(doctor);
    const expectedResult = 'Ana Soto';
    expect(actualResult).toBe(expectedResult);
  });

  it('#searchSchedules, should get schedules correctly', () => {
    medicalAppointmentComponent.doctorSchedule$ = of([
      {available: true, end_time: '10:30:00', start_time: '10:00:00'},
      {available: true, end_time: '11:00:00', start_time: '10:30:00'},
    ]
  );
    medicalAppointmentComponent.searchSchedules();
    medicalAppointmentComponent.doctorSchedule$.subscribe(x =>
      expect(x).toEqual([
        {available: true, end_time: '10:30:00', start_time: '10:00:00'},
        {available: true, end_time: '11:00:00', start_time: '10:30:00'},
      ])
    );
  });

  it('#searchSchedules, should get schedules incorrectly', () => {
    schedulesServiceMock.searchSchedules.and.returnValue(
      throwError('An error occurred')
    );
    medicalAppointmentComponent.searchSchedules();
    const actualResult = medicalAppointmentComponent.showMessage.type;
    const expectedResult: TypeAlertEnum = TypeAlertEnum.error;
    expect(actualResult).toBe(expectedResult);
  });

  it('#saveMedicalAppointment, should save a medical appointment correctly', () => {
    medicalAppointmentComponent.medicalAppointment.id_users = 1;
    medicalAppointmentComponent.medicalAppointment.attention_date = '12-03-2020';
    medicalAppointmentComponent.saveMedicalAppointment();
    const actualResult = medicalAppointmentComponent.medicalAppointment.attention_date;
    const expectedResult = '12-03-2020';
    expect(actualResult).toEqual(expectedResult);
  });

  it('#saveMedicalAppointment, should save a medical appointment incorrectly', () => {
    medicalAppointmentServiceMock.createMedicalAppointment.and.returnValue(
      throwError('An error occurred')
    );
    medicalAppointmentComponent.saveMedicalAppointment();
    const actualResult = medicalAppointmentComponent.showMessage.type;
    const expectedResult: TypeAlertEnum = TypeAlertEnum.error;
    expect(actualResult).toBe(expectedResult);
  });

});
