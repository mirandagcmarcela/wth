import { Component, OnInit } from '@angular/core';
import { Specialty } from 'src/app/models/specialty.model';
import { SpecialtyService } from 'src/app/services/specialty.service';
import { MedicalAppointment } from './../../../../models/medical-appointment.model';
import { ShowAlert } from './../../../../models/show-alert';
import { Doctor } from 'src/app/models/doctor.model';
import { DoctorService } from 'src/app/services/doctor.service';
import { DoctorSchedule } from 'src/app/models/doctor-schedule.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { MedicalAppointmentService } from 'src/app/services/medical-appointment.service';
import { SchedulesService } from 'src/app/services/schedule.service';
import { User } from 'src/app/models/user.model';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-medical-appointment',
  templateUrl: './medical-appointment.component.html',
  styleUrls: ['./medical-appointment.component.css']
})
export class MedicalAppointmentComponent implements OnInit {
  public specialties$: Observable<Specialty[]>;
  public doctors$: Observable<Doctor[]>;
  public doctorSchedule$: Observable<DoctorSchedule[]>;
  public medicalAppointment: MedicalAppointment = {
    id_medicalappointment: 0,
    attention_date: '',
    schedule: '',
    id_users: 0,
    id_doctor: 0
  };
  public appointmentForm = new FormGroup({
    date: new FormControl(''),
    specialty: new FormControl(''),
    doctor: new FormControl(''),
    start_time: new FormControl(''),
    available: new FormControl(false)
  });
  private user: User;
  private idSpecialty: any = 0;
  public showMessage: ShowAlert = new ShowAlert();

  ngOnInit() {
    this.createAppointmentForm();
    this.searchSpecialties();
    this.user = this.userService.getCurrentUser();
  }

  constructor(
    private specialtyService: SpecialtyService,
    private doctorService: DoctorService,
    private userService: UserService,
    private medicalAppointmentService: MedicalAppointmentService,
    private schedulesService: SchedulesService
  ) {}

  public searchSpecialties(): void {
    this.specialties$ = this.specialtyService.searchSpecialties();
    if (!this.specialties$) {
      this.showMessage.showMessageError('There are no specialties available');
    }
  }

  public searchDoctors(): void {
    this.idSpecialty = this.appointmentForm.controls.specialty.value;
    if (this.idSpecialty === undefined) {
      return;
    }
    this.doctors$ = this.doctorService.searchDoctors(this.idSpecialty);
    if (!this.doctors$) {
      this.showMessage.showMessageError('There are no doctors available');
    }
  }

  public searchSchedules(): void {
    this.medicalAppointment.id_doctor = this.appointmentForm.controls.doctor.value;
    this.medicalAppointment.attention_date = this.appointmentForm.controls.date.value;
    if (this.medicalAppointment.id_doctor === undefined && this.medicalAppointment.attention_date !=='') {
      return;
    }
    this.doctorSchedule$ = this.schedulesService.searchSchedules(this.medicalAppointment.id_doctor, this.medicalAppointment.attention_date);
    if (!this.doctorSchedule$) {
      this.showMessage.showMessageError('There are no schedules available');
    }
  }

  public saveMedicalAppointment(): void {
    this.medicalAppointment.id_users = this.user.id_user;
    this.medicalAppointment.schedule = this.appointmentForm.controls.start_time.value;
    this.medicalAppointmentService.createMedicalAppointment(this.medicalAppointment).subscribe(
      (response: MedicalAppointment) => {
        this.medicalAppointment = response;
        this.showMessage.showMessageSuccess('Medical Appointment registered successfully..');
      },
      (error: HttpErrorResponse) => {
        this.showMessage.showMessageError(error.error.message_error);
      }
    );
  }

  public createAppointmentForm(): void {
    this.appointmentForm = new FormGroup({
      date: new FormControl('', [
        Validators.minLength(10),
        Validators.maxLength(10),
        Validators.required
      ]),
      specialty: new FormControl('', [Validators.required]),
      doctor: new FormControl('', [Validators.required]),
      start_time: new FormControl('', [
        Validators.minLength(5),
        Validators.maxLength(5),
        Validators.required
      ]),
      available: new FormControl('', [Validators.required])
    });
  }

  public getFullNameDoctor(doctor: Doctor): string {
    return doctor.first_name + ' ' + doctor.last_name;
  }
}