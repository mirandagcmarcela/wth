import { Component } from '@angular/core';
import { RegisterService } from 'src/app/services/register.service';
import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { ShowAlert } from 'src/app/models/show-alert';
import { Person } from 'src/app/models/person.model';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-form-register',
  templateUrl: './form-register.component.html',
  styleUrls: ['./form-register.component.css']
})
export class FormRegisterComponent {
  public userForm: FormGroup;
  private person: Person = {
    id_person: 0,
    first_name: '',
    last_name: '',
    birthday: '',
    gender: '',
    phone: 0,
    address_email: ''
  };
  private user: User = {
    id_user: 0,
    id_role: 4,
    username: '',
    password: '',
    person: this.person
  };

  public showMessage = new ShowAlert();

  constructor(
    private registerService: RegisterService,
    private router: Router
  ) {
    this.createUserForm();
  }

  private createUserForm(): void {
    this.userForm = new FormGroup(
      {
        first_name: new FormControl('', [
          Validators.minLength(3),
          Validators.maxLength(20),
          Validators.required
        ]),
        last_name: new FormControl('', [
          Validators.minLength(3),
          Validators.maxLength(30),
          Validators.required
        ]),
        birthday: new FormControl('', [
          Validators.minLength(10),
          Validators.maxLength(10),
          Validators.required
        ]),
        gender: new FormControl('', [
          Validators.minLength(1),
          Validators.maxLength(1),
          Validators.required
        ]),
        phone: new FormControl('', [
          Validators.minLength(8),
          Validators.maxLength(11),
          Validators.required,
          Validators.pattern('^[0-9]*$')
        ]),
        address_email: new FormControl('', [
          Validators.minLength(10),
          Validators.maxLength(70),
          Validators.required,
          Validators.email
        ]),
        username: new FormControl('', [
          Validators.minLength(5),
          Validators.maxLength(15),
          Validators.required
        ]),
        password: new FormControl('', [
          Validators.required,
          Validators.minLength(6)
        ]),
        repeatPassword: new FormControl('', [Validators.required])
      },
      { validators: this.validatePassword }
    );
  }

  validatePassword(controlPassword: FormGroup): { invalid: boolean } {
    if (
      controlPassword.controls.password.value !==
      controlPassword.controls.repeatPassword.value
    ) {
      return { invalid: true };
    }
  }

  public saveUser(): void {
    this.saveData();
    this.registerService.createUser(this.user).subscribe(
      (response: number) => {
        this.openLogin();
        this.showMessage.showMessageSuccess('User registered successfully..');
      },
      (error: HttpErrorResponse) => {
        this.showMessage.showMessageError(error.error.message_error);
      }
    );
  }

  openLogin(): void {
    this.router.navigate(['login']);
  }

  private saveData(): void {
    this.user.username = this.userForm.controls.username.value;
    this.user.password = this.userForm.controls.password.value;
    this.person.first_name = this.userForm.controls.first_name.value;
    this.person.last_name = this.userForm.controls.last_name.value;
    this.person.birthday = this.userForm.controls.birthday.value;
    this.person.gender = this.userForm.controls.gender.value;
    this.person.phone = this.userForm.controls.phone.value;
    this.person.address_email = this.userForm.controls.address_email.value;
    this.user.person = this.person;
  }
}
