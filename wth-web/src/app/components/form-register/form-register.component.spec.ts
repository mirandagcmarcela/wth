import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FormRegisterComponent } from './form-register.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { RegisterService } from 'src/app/services/register.service';
import { Router } from '@angular/router';
import { of, throwError } from 'rxjs';
import { TypeAlertEnum } from 'src/app/models/TypeAlertEnum.model';

describe('FormRegisterComponent', () => {
  let component: FormRegisterComponent;
  let fixture: ComponentFixture<FormRegisterComponent>;
  let router;
  let navigateSpy;
  let registerServiceMock;

  beforeEach(async(() => {
    registerServiceMock = jasmine.createSpyObj('RegisterService', [
      'createUser'
    ]);
    registerServiceMock.createUser.and.returnValue(of(1));

    TestBed.configureTestingModule({
      declarations: [FormRegisterComponent],
      imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      providers: [{ provide: RegisterService, useValue: registerServiceMock }]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(FormRegisterComponent);
        component = fixture.componentInstance;
        router = TestBed.get(Router);
        navigateSpy = spyOn(router, 'navigate');
      });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('#constructor, should create a register component', () => {
    expect(component).toBeTruthy();
  });

  it('#userForm, should be a invalid register', () => {
    expect(component.userForm.valid).toBeFalsy();
  });

  it('#userForm, should be a valid register', () => {
    component.userForm.setValue({
      username: 'admin123',
      password: 'admin123',
      repeatPassword: 'admin123',
      first_name: 'Ernesto',
      last_name: 'Camacho',
      birthday: '1997-01-01',
      gender: 'M',
      phone: 65246186,
      address_email: 'ernesto.camacho@gmail.com'
    });
    expect(component.userForm.valid).toBeTruthy();
  });

  it('#saveUser, should be save a user correctly', () => {
    component.saveUser();
    const actualResult = component.showMessage.type;
    const expectedResult: TypeAlertEnum = TypeAlertEnum.success;
    expect(actualResult).toBe(expectedResult);
  });

  it('#saveUser, should be save a user incorrectly', () => {
    registerServiceMock.createUser.and.returnValue(
      throwError('Username already exists.')
    );
    component.saveUser();
    const actualResult = component.showMessage.type;
    const expectedResult: TypeAlertEnum = TypeAlertEnum.error;
    expect(actualResult).toBe(expectedResult);
  });

  it('#openLogin, should move to the login window', () => {
    component.openLogin();
    expect(navigateSpy).toHaveBeenCalledWith(['login']);
  });
});
