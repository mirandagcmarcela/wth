import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { of, throwError } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { HeaderComponent } from './header.component';
import { MenuService, MenuButton } from 'src/app/services/menu.service';
import { UserService } from 'src/app/services/user.service';
import { TypeAlertEnum } from 'src/app/models/TypeAlertEnum.model';

describe('HeaderComponent', () => {
  let headerComponent: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let router;
  let navigateSpy;
  let userServiceMock;
  let loginServiceMock;
  let menuServiceMock;
  const user: User = {
    id_user: 0,
    id_role: 4,
    username: '',
    password: '',
    person: {
      id_person: 0,
      first_name: '',
      last_name: '',
      birthday: '',
      gender: '',
      phone: 0,
      address_email: ''
    }
  };
  const btn: MenuButton = {
    route: 'patient',
    name: 'Home',
    icon: 'fa fa-home'
  };
  const btns: MenuButton[] = [];
  btns[0] = btn;

  beforeEach(async(() => {
    loginServiceMock = jasmine.createSpyObj('LoginService', ['searchUser']);
    loginServiceMock.searchUser.and.returnValue(of(user));
    
    menuServiceMock = jasmine.createSpyObj('MenuService', ['setMenu']);
    menuServiceMock.setMenu.and.returnValue(of(btns));

    userServiceMock = jasmine.createSpyObj('UserService', ['getCurrentUser']);
    userServiceMock.getCurrentUser.and.returnValue(true);

    TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      providers: [
        { provide: LoginService, useValue: loginServiceMock },
        { provide: MenuService, useValue: menuServiceMock },
        { provide: UserService, useValue: userServiceMock }
      ]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(HeaderComponent);
        headerComponent = fixture.componentInstance;

        router = TestBed.get(Router);
        navigateSpy = spyOn(router, 'navigate');
      });
  }));

  it('#constructor, should create a login component', () => {
    expect(headerComponent).toBeTruthy();
  });

  it('#changeDisplay, should have a parameter displaySignInModal true', () => {
    expect(headerComponent.changeDisplay(true)).toBeUndefined();
  });

  it('#loginForm, should be a invalid login', () => {
    expect(headerComponent.loginForm.valid).toBeFalsy();
  });

  it('#loginForm, should be a valid login', () => {
    headerComponent.loginForm.setValue({
      username: 'admin',
      password: 'admin'
    });
    expect(headerComponent.loginForm.valid).toBeTruthy();
  });

  it('#login, should be login correctly', () => {
    headerComponent.login();
    const actualResult = headerComponent.showMessage.type;
    const expectedResult: TypeAlertEnum = TypeAlertEnum.success;
    expect(actualResult).toBe(expectedResult);
  });

  it('#login, should be login uncorrectly', () => {
    loginServiceMock.searchUser.and.returnValue(
      throwError('Username or password incorrect'));

    headerComponent.login();
    const actualResult = headerComponent.showMessage.type;
    const expectedResult: TypeAlertEnum = TypeAlertEnum.error;
    expect(actualResult).toBe(expectedResult);
  });

  it('#openPatientHome, should move to the patientHome window', () => {
    headerComponent.openPatientHome();
    expect(navigateSpy).toHaveBeenCalledWith(['patient']);
  });

  it('#openLogin, should move to the login window', () => {
    headerComponent.openLogin();
    expect(navigateSpy).toHaveBeenCalledWith(['login']);
  });

  it('#sessionUser, should verify if exists a user', () => {
    headerComponent.sessionUser();
    const actualResult = headerComponent.sessionUser();
    expect(actualResult).toBeTruthy();
  });

});
