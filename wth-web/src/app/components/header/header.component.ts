import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { isNullOrUndefined } from 'util';
import { MenuService, MenuButton } from 'src/app/services/menu.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { ShowAlert } from 'src/app/models/show-alert';

@Component({
  selector: 'app-header',
  styleUrls: ['header.component.css'],
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  user: User;
  listOfButtons$: Observable<MenuButton[]>;
  public displaySignInModal = false;
  public loginForm: FormGroup;
  public showMessage = new ShowAlert();

  constructor(
    private loginService: LoginService,
    private userService: UserService,
    private menuService: MenuService,
    private router: Router
  ) {
    this.createLoginForm();
  }

  ngOnInit() {
    this.listOfButtons$ = this.menuService.menuButton$;
  }

  openLogin(): void {
    this.router.navigate(['login']);
  }

  logOut() {
    this.userService.removeCurrentUser();
    this.openLogin();
  }

  changeDisplay(display: boolean): void {
    this.displaySignInModal = display;
  }

  openPatientHome(): void {
    this.router.navigate(['patient']);
  }

  sessionUser() {
    this.user = this.userService.getCurrentUser();
    if (isNullOrUndefined(this.user)) {
      return false;
    }
    return true;
  }

  createLoginForm(): void {
    this.loginForm = new FormGroup({
      username: new FormControl('', [
        Validators.minLength(5),
        Validators.maxLength(10),
        Validators.required
      ]),
      password: new FormControl('', [Validators.required])
    });
  }

  login(): void {
    this.loginService.searchUser(this.loginForm.value).subscribe(
      (response: User) => {
        this.user = response;
        this.userService.setCurrentUser(this.user);
        this.showMessage.showMessageSuccess('Login Successfull');
        this.displaySignInModal = false;
        this.openPatientHome();
      },
      (error: HttpErrorResponse) => {
        this.showMessage.showMessageError(error.error.message_error);
      }
    );
  }
}
