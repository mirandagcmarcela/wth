import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { User } from './../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  constructor(private http: HttpClient) {}

  createUser(user: User): Observable<number> {
    return this.http.post<number>(`${environment.urlApi}/register`, user);
  }
}
