import { Injectable } from '@angular/core';
import { MedicalAppointment } from './../models/medical-appointment.model';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MedicalAppointmentService {
  constructor(private http: HttpClient) {}

  public createMedicalAppointment(medicalAppointment: MedicalAppointment): Observable<MedicalAppointment> {
    return this.http.post<MedicalAppointment>(`${environment.urlApi}/appointment`, medicalAppointment);
  }
}
