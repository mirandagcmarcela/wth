import { TestBed } from '@angular/core/testing';

import { DoctorService } from './doctor.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

describe('DoctorService', () => {
  let doctorService: DoctorService;
  const id_specialty = 1;
  let spy: any;
  let doctors = [
      {first_name: 'Ana', id_doctor: 1, last_name: 'Pererira'},
      {first_name: 'Pablo', id_doctor: 2, last_name: 'Flores'}
    ];

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [DoctorService]})
    .compileComponents()
    .then(() => {
      doctorService = TestBed.get(DoctorService);
      })
  );

  it('#constructor, should be created', () => {
    expect(doctorService).toBeTruthy();
  });

  it('#searchDoctors, should be receive a id_specialty', () => {
    expect(doctorService.searchDoctors(id_specialty)).toBeTruthy();
  });

  it('#searchDoctors, should return an observable with a list of doctors', () => {
    spy = spyOn(doctorService, 'searchDoctors').and.returnValue(of(doctors));
    doctorService.searchDoctors(id_specialty).subscribe(x =>
      expect(x).toEqual(doctors)
    );
  });

});
