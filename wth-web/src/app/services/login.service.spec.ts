import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LoginService } from './login.service';
import { User } from 'src/app/models/user.model';
import { of } from 'rxjs';

describe('LoginService', () => {
  let loginService: LoginService;
  let spy: any;
  let user: User = {
    id_user: 1,
    id_role: 4,
    username: 'admin',
    password: 'admin',
    person: {
      id_person: 0,
      first_name: '',
      last_name: '',
      birthday: '',
      gender: '',
      phone: 0,
      address_email: ''
    }
  };
  
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [LoginService]
    })
      .compileComponents()
      .then(() => {
        loginService = TestBed.get(LoginService);
      })
  );

  it('#constructor, should be created a login service', () => {
    expect(loginService).toBeTruthy();
  });

  it('#searchUser, should be receive a user', () => {
    expect(loginService.searchUser(user)).toBeTruthy();
  });

  it('#searchUser, should return an observable with an user', () => {
    spy = spyOn(loginService, 'searchUser').and.returnValue(of(user));
    loginService.searchUser(user).subscribe(x =>
      expect(x).toEqual(user)
    );
  });
});
