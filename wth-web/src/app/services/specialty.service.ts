import { Injectable } from '@angular/core';
import { Specialty } from './../models/specialty.model';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SpecialtyService {
    constructor(private http: HttpClient) { }

    public searchSpecialties(): Observable<Specialty[]> {
        return this.http.get<Specialty[]>(`${environment.urlApi}/specialties`);
    }
}
