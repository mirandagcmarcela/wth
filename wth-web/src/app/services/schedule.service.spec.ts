import { TestBed } from '@angular/core/testing';

import { SchedulesService } from './schedule.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

describe('SchedulesService', () => {
  let schedulesService: SchedulesService;
  const id_doctor = 1;
  const date = '15-03-2020';
  let spy: any;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [SchedulesService]})
    .compileComponents()
    .then(() => {
      schedulesService = TestBed.get(SchedulesService);
      })
  );

  it('#constructor, should be created', () => {
    expect(schedulesService).toBeTruthy();
  });

  it('#searchService, should be receive a date and an id_doctor', () => {
    expect(schedulesService.searchSchedules(id_doctor, date)).toBeTruthy();
  });

  it('#searchSchedules, should return a observable', () => {
    spy = spyOn(schedulesService, 'searchSchedules').and.returnValue(of([
      {available: true, end_time: '10:30:00', start_time: '10:00:00'},
      {available: true, end_time: '11:00:00', start_time: '10:30:00'},
    ]));
    schedulesService.searchSchedules(id_doctor, date).subscribe(x =>
      expect(x).toEqual([
        {available: true, end_time: '10:30:00', start_time: '10:00:00'},
        {available: true, end_time: '11:00:00', start_time: '10:30:00'},
      ])
    );
  });

});
