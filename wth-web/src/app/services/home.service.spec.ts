import { TestBed } from '@angular/core/testing';

import { HomeService } from './home.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('HomeService', () => {
  let  homeService: HomeService;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [HomeService]})
    .compileComponents()
    .then(() => {
        homeService = TestBed.get(HomeService);
      })
  );

  it('#constructor, should be created', () => {
    expect(homeService).toBeTruthy();
  });
});
