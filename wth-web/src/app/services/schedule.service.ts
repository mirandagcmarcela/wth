import { Injectable } from '@angular/core';
import { DoctorSchedule } from './../models/doctor-schedule.model';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SchedulesService {
  constructor(private http: HttpClient) {}
  public searchSchedules(id_doctor: number, date: string): Observable<DoctorSchedule[]> {
    return this.http.get<DoctorSchedule[]>(`${environment.urlApi}/doctor/${id_doctor}/schedule?date=${date}`);
  }
}
