import { TestBed } from '@angular/core/testing';

import { SpecialtyService } from './specialty.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

describe('SpecialtyService', () => {
  let specialtyService: SpecialtyService;
  let spy: any;
  let specialties = [
    {id_specialty: 1, name: 'Cardiology'},
    {id_specialty: 2, name: 'Urology'}
    ];

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [SpecialtyService]})
    .compileComponents()
    .then(() => {
      specialtyService = TestBed.get(SpecialtyService);
      })
  );

  it('#constructor, should be created', () => {
    expect(specialtyService).toBeTruthy();
  });

  it('#searchSpecialties, should return an observable with a list of specialties', () => {
    spy = spyOn(specialtyService, 'searchSpecialties').and.returnValue(of(specialties));
    specialtyService.searchSpecialties().subscribe(x =>
      expect(x).toEqual(specialties)
    );
  });
});
