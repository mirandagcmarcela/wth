import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RegisterService } from './register.service';
import { User } from 'src/app/models/user.model';
import { of } from 'rxjs';

describe('RegisterService', () => {
  let registerService: RegisterService;
  let spy: any;
  let id_user = 2;
  let user: User = {
    id_user: 2,
    id_role: 4,
    username: 'admin',
    password: 'admin',
    person: {
      id_person: 0,
      first_name: '',
      last_name: '',
      birthday: '',
      gender: '',
      phone: 0,
      address_email: ''
      } 
      };
  
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [RegisterService]
    })
      .compileComponents()
      .then(() => {
        registerService = TestBed.get(RegisterService);
      }
      )
  );

  it('#constructor, should be created a register service', () => {
    expect(registerService).toBeTruthy();
  });

  it('#createUser, should be receive a user', () => {
    expect(registerService.createUser(user)).toBeTruthy();
  });

  it('#createUser, should return an observable with an user id', () => {
    spy = spyOn(registerService, 'createUser').and.returnValue(of(id_user));
    registerService.createUser(user).subscribe(x =>
      expect(x).toEqual(id_user)
    );
  });

});
