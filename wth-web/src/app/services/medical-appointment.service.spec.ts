import { TestBed } from '@angular/core/testing';
import { MedicalAppointment } from './../models/medical-appointment.model';
import { MedicalAppointmentService } from './medical-appointment.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

describe('MedicalAppointmentService', () => {
  let appointment: MedicalAppointment;
  let medicalAppointmentService: MedicalAppointmentService;
  let spy: any;
  let medicalAppointmentWithId = 
  {
    id_medicalappointment: 1,
    attention_date: '10-03-2020',
    schedule: '10:30',
    id_users: 5,
    id_doctor: 2
  }
  let medicalAppointmentWithoutId = 
  {
    id_medicalappointment: 0,
    attention_date: '10-03-2020',
    schedule: '10:30',
    id_users: 5,
    id_doctor: 2
  }

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [MedicalAppointmentService]})
    .compileComponents()
    .then(() => {
      medicalAppointmentService = TestBed.get(MedicalAppointmentService);
      })
  );

  it('#constructor, should be created', () => {
    expect(medicalAppointmentService).toBeTruthy();
  });

  it('#createMedicalAppointment, should be receive a appointment', () => {
    expect(medicalAppointmentService.createMedicalAppointment(appointment)).toBeTruthy();
  });

  it('#createMedicalAppointment, should return an observable with a medical appointment id', () => {
    spy = spyOn(medicalAppointmentService, 'createMedicalAppointment').and.returnValue(of(
      medicalAppointmentWithId));
    medicalAppointmentService.createMedicalAppointment(medicalAppointmentWithoutId).subscribe(x =>
      expect(x).toEqual(medicalAppointmentWithId)
    );
  });
  
});
