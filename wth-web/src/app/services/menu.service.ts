import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MenuButton {
  route: string;
  name: string;
  icon: string;
}

export class MenuService {
  menuButton$: Subject<MenuButton[]> = new Subject<MenuButton[]>();
  constructor() {}

  public setMenu(buttonList: MenuButton[]): void {
    this.menuButton$.next(buttonList);
  }
}
