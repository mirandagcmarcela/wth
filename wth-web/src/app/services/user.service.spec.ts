import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserService } from './user.service';
import { User } from '../models/user.model';

describe('UserService', () => {
  let userService: UserService;
  let user: User = {
          id_user: 0,
          id_role: 4,
          username: 'admin',
          password: 'admin',
          person: {
            id_person: 0,
            first_name: '',
            last_name: '',
            birthday: '',
            gender: '',
            phone: 0,
            address_email: ''
          }
        };;

  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService]
    })
      .compileComponents()
      .then(() => {
        userService = TestBed.get(UserService);
      })
  );

  it('#constructor, should be created', () => {
    const userServiceC: UserService = TestBed.get(UserService);
    expect(userServiceC).toBeTruthy();
  });

  it('#getCurrentUser, should have getCurrentUser method', () => {
    expect(userService.getCurrentUser).toBeTruthy();
  });

  it('#removeCurrentUser, should have removeCurrentUser method', () => {
    expect(userService.removeCurrentUser).toBeTruthy();
  });

  it('#setCurrentUser, should have setCurrentUser method', () => {
    expect(userService.setCurrentUser).toBeTruthy();
  });

  it('#getCurrentUser, should be exists a user', () => {
    userService.setCurrentUser(user);
    expect(userService.getCurrentUser()).not.toBeNull();
  });

  it('#getCurrentUser, should be null the user', () => {
    userService.removeCurrentUser();
    expect(userService.getCurrentUser()).toBeUndefined();
  });
});
