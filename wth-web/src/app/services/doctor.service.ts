import { Injectable } from '@angular/core';
import { Doctor } from './../models/doctor.model';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {
  constructor(private http: HttpClient) { }
  public searchDoctors(id_specialty: number): Observable<Doctor[]> {
    return this.http.get<Doctor[]>(`${environment.urlApi}/specialties/${id_specialty}/doctors`);
  }
}
