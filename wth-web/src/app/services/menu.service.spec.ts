import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MenuService, MenuButton } from './menu.service';
import { of } from 'rxjs';

describe('MenuService', () => {
  let menuService: MenuService;
  let btn: MenuButton;
  const buttons: MenuButton[] = [];
  beforeEach(async () =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [MenuService]
    })
      .compileComponents()
      .then(() => {
        btn = {
          route: 'patient',
          name: 'Home',
          icon: 'fa fa-home'
        };
        buttons[0] = btn;
        buttons[1] = btn;
        menuService = TestBed.get(MenuService);
      })
  );

  it('#constructor, should be created', () => {
    expect(menuService).toBeTruthy();
  });

  it('#SetMenu, should sent a value to the subject', () => {
    menuService.menuButton$.subscribe(x =>
      expect(x[0].route).toEqual(btn.route)
    );
  });
});
