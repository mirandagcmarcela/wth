import { Injectable } from '@angular/core';
import { User } from './../models/user.model';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private http: HttpClient) {}

  public searchUser(user: User): Observable<User> {
    return this.http.post<User>(`${environment.urlApi}/login`, user);
  }
}
