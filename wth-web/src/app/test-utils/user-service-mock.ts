import { User } from 'src/app/models/user.model';
import { Person } from '../models/person.model';

export class UserServiceMock {
  private person: Person = {
    id_person: 0,
    first_name: '',
    last_name: '',
    birthday: '',
    gender: '',
    phone: 0,
    address_email: ''
  };
  private user: User = {
    id_user: 0,
    id_role: 4,
    username: 'admin',
    password: 'admin',
    person: this.person
  };

  /**
   * This method return a object user
   */
  getCurrentUser() {
    return this.user;
  }
}
