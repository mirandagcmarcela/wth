import { AuthGuard } from './auth.guard';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { LoginService } from '../services/login.service';
import { UserService } from '../services/user.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserServiceMock } from '../test-utils/user-service-mock';
import { User } from 'src/app/models/user.model';

describe('AuthGuard', () => {
  let authGuard: AuthGuard;
  let userService: UserService;
  const user: User = {
    id_user: 0,
    id_role: 4,
    username: '',
    password: '',
    person: {
      id_person: 0,
      first_name: '',
      last_name: '',
      birthday: '',
      gender: '',
      phone: 0,
      address_email: ''
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: userService,
          useClass: UserServiceMock
        },
        AuthGuard,
        LoginService
      ],
      imports: [RouterTestingModule, HttpClientTestingModule]
    });
    authGuard = TestBed.get(AuthGuard);
    userService = TestBed.get(UserService);
  });

  it('#constructor, should create an instance', () => {
    expect(authGuard).toBeTruthy();
  });

  it('#canActivate ,should have canActivate method', () => {
    expect(authGuard.canActivate).toBeTruthy();
  });

  it('#canActivate, should be the answer canActivate method false, if does not exists a user', () => {
    userService.removeCurrentUser();
    expect(authGuard.canActivate()).toBeFalsy();
  });

  it('#canActivate, should be the answer canActivate method true, if exists a user', () => {
    userService.setCurrentUser(user);
    expect(authGuard.canActivate()).toBeTruthy();
  });
});
