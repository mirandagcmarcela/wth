import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// Routes
import { AppRoutingModule } from './app.routes';

// Services
import { LoginService } from './services/login.service';
import { UserService } from './services/user.service';
import { RegisterService } from './services/register.service';
import { SpecialtyService } from './services/specialty.service';
import { DoctorService } from './services/doctor.service';
import { MedicalAppointmentService } from './services/medical-appointment.service';
import { SchedulesService } from './services/schedule.service';

// Components
import { LoginComponent } from './components/login/login.component';
import { AppComponent } from './app.component';
import { FormRegisterComponent } from './components/form-register/form-register.component';
import { HeaderComponent } from './components/header/header.component';
import { MenuService } from './services/menu.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    FormRegisterComponent
  ],
  imports: [
    AppRoutingModule,
    HttpClientModule,
    BrowserModule,
    ReactiveFormsModule,
    FormsModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    LoginService,
    UserService,
    RegisterService,
    DoctorService,
    MenuService,
    MedicalAppointmentService,
    SchedulesService,
    SpecialtyService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
