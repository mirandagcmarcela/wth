import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { NgModule } from '@angular/core';
import { AuthGuard } from './guards/auth.guard';
import { FormRegisterComponent } from './components/form-register/form-register.component';

export const APP_ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    canActivate: [AuthGuard],
    path: 'patient',
        loadChildren: () =>
          import('./components/patient/patient.module').then(
            m => m.PatientModule)
  },
  {
    path: 'register',
    component: FormRegisterComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '/login'
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(APP_ROUTES, {
      preloadingStrategy: PreloadAllModules
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
