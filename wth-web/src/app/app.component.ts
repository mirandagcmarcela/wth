import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Way To Heaven';

  constructor(private router: Router) {}

  openPatientHome(): void {
    this.router.navigate(['patient']);
  }
}
