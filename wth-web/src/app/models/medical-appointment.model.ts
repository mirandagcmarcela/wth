export interface MedicalAppointment {
  id_medicalappointment: number;
  attention_date: string;
  schedule: string;
  id_users: number;
  id_doctor: number;
}
