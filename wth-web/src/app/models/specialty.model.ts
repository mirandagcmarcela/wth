export interface Specialty {
  id_specialty: number;
  name: string;
}
