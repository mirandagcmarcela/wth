export interface DoctorSchedule {
  available: boolean;
  end_time: string;
  start_time: string;
}
