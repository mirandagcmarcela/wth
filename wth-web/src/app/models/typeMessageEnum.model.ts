export enum TypeMessageEnum {
    warning = 'alert alert-warning',
    danger = 'alert alert-danger',
    success = 'alert alert-success',
    info = 'alert alert-info'
}
