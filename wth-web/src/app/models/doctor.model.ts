export interface Doctor {
  first_name: string;
  id_doctor: number;
  last_name: string;
}
