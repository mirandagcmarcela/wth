export interface Person {
    id_person: number;
    first_name: string;
    last_name: string;
    birthday: string;
    gender: string;
    phone: number;
    address_email: string;
}
