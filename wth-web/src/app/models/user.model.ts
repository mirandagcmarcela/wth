import { Person } from './person.model';

export interface User {
  id_user: number;
  id_role: number;
  username: string;
  password: string;
  person: Person;
}
