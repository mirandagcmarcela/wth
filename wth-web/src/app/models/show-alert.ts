import Swal from 'sweetalert2';
import { TypeAlertEnum } from './TypeAlertEnum.model';

export class ShowAlert {
  public message = '';
  public type: TypeAlertEnum = TypeAlertEnum.error;

  public showMessageSuccess(message: string) {
    this.message = message;
    this.type = TypeAlertEnum.success;
    Swal.fire({
      position: 'center',
      icon: this.type,
      title: message,
      showConfirmButton: false,
      timer: 1500
    });
  }

  public showMessageError(message: string) {
    this.message = message;
    Swal.fire({
      position: 'center',
      icon: this.type,
      title: message,
      showConfirmButton: false,
      timer: 1500
    });
  }
}
